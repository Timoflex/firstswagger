package com.exampleswagger.demo.service;

import com.exampleswagger.demo.model.ToDoModel;
import com.exampleswagger.demo.repository.ToDoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ToDoService {
    private final ToDoRepository ToDoRepository;

    public void add (ToDoModel model) {ToDoRepository.add(model);}

    public void delete (Integer ObjId) {ToDoRepository.delete(ObjId);}

    public void change (Integer ObjId, ToDoModel model) {ToDoRepository.change(ObjId, model);}

    public List<ToDoModel> getList() {return ToDoRepository.getList();}
}
