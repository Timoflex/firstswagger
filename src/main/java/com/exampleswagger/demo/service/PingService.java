package com.exampleswagger.demo.service;

import com.exampleswagger.demo.model.PingModel;
import com.exampleswagger.demo.repository.PingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class PingService {

    private final PingRepository pingRepository;

    public PingModel ping(){
        var ping = new PingModel();
        ping.setCode(200);
        ping.setMessage("ok");
        return ping;
    }

    public void add(PingModel model){ pingRepository.add(model); }

    public List<PingModel> getList(){
        return pingRepository.getList();
    }
}
