package com.exampleswagger.demo.controller;


import com.exampleswagger.demo.model.ToDoModel;
import com.exampleswagger.demo.service.ToDoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ToDoController {

    private final ToDoService ToDoService; //инектим сервисный слой (@RequiredArgsConstructor)


    @PostMapping("/add Objective")
    public void add (ToDoModel model) { ToDoService.add(model); }

    @DeleteMapping("/delete Objective")
    public void delete (Integer ObjId) {ToDoService.delete(ObjId);}

    @GetMapping("/getlist")
    public List<ToDoModel> getList() { return ToDoService.getList(); }

    @PatchMapping ("/change Objective")
    public void change (Integer ObjId, ToDoModel model) {ToDoService.change(ObjId, model);}

}