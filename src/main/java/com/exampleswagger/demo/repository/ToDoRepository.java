package com.exampleswagger.demo.repository;

import com.exampleswagger.demo.model.ToDoModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ToDoRepository {
    private final List<ToDoModel> storage = new ArrayList<>();

    public void add(ToDoModel model){
        storage.add(model);
    }

    public void delete(Integer ObjId){
        int i = ObjId;
        storage.remove(i);
    }

    public void change (Integer ObjId, ToDoModel model){
        storage.set(ObjId, model);
    }

    public List<ToDoModel> getList(){
        return storage;
    }

}
